﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Fincas.Models;

namespace Fincas.Controllers
{
    public class FamiliarController : Controller
    {
        private fincasEntities db = new fincasEntities();

        // GET: Personas
        public ActionResult AdministrarFamiliares(int idPersona,int idFinca)
        {
            List<Familiar> familiares = db.Familiars.Where(f => f.codigo_persona == idPersona).ToList();
            List<Persona> personas = new List<Persona>();
            List<String> parentescos = new List<String>();
            foreach (Familiar item in familiares)
            {
                personas.Add(db.Personas.Find(item.codigo_familiar));
                parentescos.Add(item.parentesco);
            }
            ViewBag.idPersona = idPersona;
            ViewBag.parentescos = parentescos;
            ViewBag.idFinca = idFinca;
            return View("Index", personas);
        }
  
        // GET: Personas/Details/5
        public ActionResult Details(int? id,int idPersona,String parentesco)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            ViewBag.idPersona = idPersona;
            ViewBag.parentesco = parentesco;
            return View(persona);
        }

        // GET: Personas/Create
        public ActionResult Create(int idPersona)
        {
            ViewBag.idPersona = idPersona;
            return View();
        }

        // POST: Personas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "codigo,tipo_identificacion,numero_identificacion,nombre,rol")] Persona persona,int idPersona,String parentesco)
        {
            if (ModelState.IsValid)
            {
                Familiar familiar = new Familiar();
                familiar.codigo_persona= idPersona;
                familiar.codigo_familiar = persona.codigo;
                familiar.parentesco = parentesco;
                db.Familiars.Add(familiar);
                db.Personas.Add(persona);
                db.SaveChanges();
                return RedirectToAction("AdministrarFamiliares", new { idPersona = idPersona });
            }

            return View(persona);
        }

        // GET: Personas/Edit/5
        public ActionResult Edit(int? id,int idPersona,String parentesco)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            ViewBag.idPersona = idPersona;
            ViewBag.parentesco = parentesco;
            return View(persona);
        }

        // POST: Personas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "codigo,tipo_identificacion,numero_identificacion,nombre,rol")] Persona persona,int idPersona)
        {
            if (ModelState.IsValid)
            {
                db.Entry(persona).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AdministrarFamiliares", new { idPersona = idPersona });
            }
            return View(persona);
        }

        // GET: Personas/Delete/5
        public ActionResult Delete(int? id,int idPersona,String parentesco)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            ViewBag.idPersona = idPersona;
            ViewBag.parentesco = parentesco;
            return View(persona);
        }

        // POST: Personas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int idPersona)
        {
            Familiar familiar = db.Familiars.Where(f => f.codigo_familiar == id).Where(f => f.codigo_persona == idPersona).First();
            db.Familiars.Remove(familiar);
            Persona persona = db.Personas.Find(id);
            db.Personas.Remove(persona);
            db.SaveChanges();
            return RedirectToAction("AdministrarFamiliares", new { idPersona = idPersona });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
