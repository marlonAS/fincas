﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Fincas.Models;

namespace Fincas.Controllers
{
    public class PersonasController : Controller
    {
        private fincasEntities db = new fincasEntities();

        // GET: Personas
        public ActionResult Index()
        {
            return View(db.Personas.ToList());
        }
        public ActionResult AdministrarPorFinca(int idFinca)
        {
           List<Finca_Persona> fincaPersonas = db.Finca_Persona.Where(p => p.codigo_finca == idFinca).ToList();
            List<Persona> personas = new List<Persona>();
            foreach(Finca_Persona item in fincaPersonas)
            {
                personas.Add(db.Personas.Find(item.codigo_persona));
            }
            ViewBag.idFinca = idFinca;
            return View("Index",personas);
        }
        
        // GET: Personas/Details/5
        public ActionResult Details(int? id, int idFinca)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            ViewBag.idFinca = idFinca;
            return View(persona);
        }

        // GET: Personas/Create
        public ActionResult Create(int? idFinca)
        {
            ViewBag.idFinca = idFinca;
            return View();
        }

        // POST: Personas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "codigo,tipo_identificacion,numero_identificacion,nombre,rol")] Persona persona,int idFinca)
        {
            if (ModelState.IsValid)
            {
                Finca_Persona fincaPersona = new Finca_Persona();
                fincaPersona.codigo_finca = idFinca;
                fincaPersona.codigo_persona = persona.codigo;
                db.Finca_Persona.Add(fincaPersona);
                db.Personas.Add(persona);
                db.SaveChanges();
                return RedirectToAction("AdministrarPorFinca", new { idFinca = idFinca });
            }

            return View(persona);
        }

        // GET: Personas/Edit/5
        public ActionResult Edit(int? id,int idFinca)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            ViewBag.idFinca = idFinca;
            return View(persona);
        }

        // POST: Personas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "codigo,tipo_identificacion,numero_identificacion,nombre,rol")] Persona persona,int idFinca)
        {
            if (ModelState.IsValid)
            {
                db.Entry(persona).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AdministrarPorFinca", new { idFinca = idFinca });
            }
            return View(persona);
        }

        // GET: Personas/Delete/5
        public ActionResult Delete(int? id,int idFinca)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            ViewBag.idFinca = idFinca;
            return View(persona);
        }

        // POST: Personas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int idFinca)
        {
            Finca_Persona fincaPersona = db.Finca_Persona.Where(fp => fp.codigo_finca == idFinca).Where(fp => fp.codigo_persona == id).First();
            db.Finca_Persona.Remove(fincaPersona);
            Persona persona = db.Personas.Find(id);
            db.Personas.Remove(persona);
            db.SaveChanges();
            return RedirectToAction("AdministrarPorFinca", new { idFinca = idFinca });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
